﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector3 movement;
    Animator animator;
    Rigidbody rigid;
    Quaternion rotation = Quaternion.identity;

    public float turnSpeed = 20f;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        //  해당 움직임의 축을 받는다
        //  키보드와 조이스틱의 입력값에 대해 (-1f ~ -1f) 사이의 값을 반환한다
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        movement.Set(horizontal, 0f, vertical);
        movement.Normalize();

        //  비슷한 2개의 부동소수점 값을 비교한다
        //  왜? 1.0f == 10.0f / 10.0f 은 같지 않을수도 있다 (특히 double 자료형)
        //  앞의 !는 값을 반대로 변경한다 (ex : !ture 는 false 로 반환)
        bool isHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool isVerticalInput = !Mathf.Approximately(vertical, 0f);

        //  둘 중 하나라도 true 라면 true 를 반환, 둘 다 아니라면 false를 반환한다
        //  한 축이라도 움직인다면 isWalking 은 true 가 된다!
        bool isWalking = isHorizontalInput || isVerticalInput;

        //  애니메이션 윈도우에서 설정한 IsWalking 값을 설정한다
        animator.SetBool("IsWalking", isWalking);

        Vector3 desiredForward = Vector3.RotateTowards(
            transform.forward,          //  향할 벡터
            movement,                   //  변경할 벡터
            turnSpeed * Time.deltaTime, //  두 벡터 사이의 거리 (라디안)
            0f);                        //  라디안의 크기

        rotation = Quaternion.LookRotation(desiredForward);
    }

    //  루트모션이 적용된 애니메이션들의 tarnsform을 직접 구현하는 함수
    //  이 함수를 선언하게 되면 애니메이션들의 루트 모션 값을 무시한다
    private void OnAnimatorMove()
    {
        rigid.MovePosition(rigid.position + movement * animator.deltaPosition.magnitude);
        rigid.MoveRotation(rotation);
    }

}
