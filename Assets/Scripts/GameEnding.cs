﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitImageGroup;

    bool isPlayerAtExit;
    float timer;

    //  콜라이더를 가지고 있는 객체가 이 컴포넌트를 지나게 되면 발동한다
    private void OnTriggerEnter(Collider other)
    {
        //  플레이어가 이 콜라이더를 지나면 Exit 값을 true 으로 설정한다
        if (other.gameObject == player)
            isPlayerAtExit = true;
    }

    private void Update()
    {
        if (isPlayerAtExit) EndLevel();
    }

    //  인스펙터에서 설정한 exitImageGroup 안의 이미지 알파값을 변경한다
    private void EndLevel()
    {
        timer += Time.deltaTime;
        exitImageGroup.alpha = timer / fadeDuration;

        if (timer > fadeDuration + displayImageDuration)
            Application.Quit();
    }
}
